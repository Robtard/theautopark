package autopark.dao.impl;

import autopark.dao.IPasswordResetTokenDAO;
import autopark.domain.PasswordResetToken;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * 21.03.2016.
 */
@Repository
public class PasswordResetTokenDAOImpl extends RootDAOImpl<PasswordResetToken> implements IPasswordResetTokenDAO {


    private final Logger log = LoggerFactory.getLogger(PasswordResetTokenDAOImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String GET_BY_TOKEN_VALUE = "SELECT * FROM ap_password_reset_token WHERE tokenValue = ?";

    private static final String GET_BY_EMAIL = "FROM PasswordResetToken WHERE email = ?";

    private static final String GET_EMAIL_BY_TOKEN_VALUE = "select email from PasswordResetToken where tokenValue = ?";

    private static final String DELETE_WHERE_TOKEN_VALUE = "delete from ap_password_reset_token where tokenValue = ?";

    public PasswordResetTokenDAOImpl() {
        super("autopark.domain.PasswordResetToken", PasswordResetToken.class);
    }

    @Override
    @Transactional
    public PasswordResetToken findOneByTokenValue(String tokenValue) {

        log.info(GET_BY_TOKEN_VALUE);

        return jdbcTemplate.queryForObject(GET_BY_TOKEN_VALUE, new Object[]{tokenValue},
                ParameterizedBeanPropertyRowMapper.newInstance(PasswordResetToken.class));
    }

    @Override
    @Transactional
    public PasswordResetToken findOneByEmail(String email) {

        /*Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        PasswordResetToken token = (PasswordResetToken) session
                .createQuery(GET_BY_EMAIL)
                .setString(0, email)
                .uniqueResult();

        session.getTransaction().commit();
        session.close();

        return token;*/
        return null;
    }

    public void delete(PasswordResetToken token){
        log.info(DELETE_WHERE_TOKEN_VALUE);

        jdbcTemplate.update(DELETE_WHERE_TOKEN_VALUE, token.getTokenValue());
    }

}
