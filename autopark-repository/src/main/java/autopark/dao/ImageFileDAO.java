package autopark.dao;


import autopark.domain.ImageFile;

public interface ImageFileDAO extends IRootDAO<ImageFile> {
}