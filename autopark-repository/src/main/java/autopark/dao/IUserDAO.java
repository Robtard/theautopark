package autopark.dao;


import autopark.domain.PasswordResetToken;
import autopark.domain.User;

public interface IUserDAO extends IRootDAO<User> {

    User getByEmail(final String email);

    User getByFacebookId(final String facebookId);


}