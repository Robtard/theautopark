<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="tiles/head.jsp" %>

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
</head>

<body>

<tags:headerV2 activeProfile="active"/>
<div id="wrap">

    <div class="container">
        <c:choose>
            <c:when test="${keymissing == 1}">
                <h2 class="form-signin-heading">The reset key is missing.</h2>
                <br/>
            </c:when>

            <c:when test="${valid == 0}">
                <h2 class="form-signin-heading">Your password couldn't be reset. Remember a password request is only
                    valid for 24 hours.</h2>
                <br/>
            </c:when>
            <c:when test="${valid == 1}">

                <springForm:form action="/lostpassword/finish/${token}" method="post" commandName="password-form"
                                 cssClass="form-lostpass">
                    <h2 class="form-signin-heading">Восстановление пароля</h2>
                    <br/>

                    <input name="password" type="password" placeholder="Введите новый пароль" class="form-control">
                    <div><springForm:errors path="password" class="alert alert-danger" role="alert"/>
                    </div>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">ОК</button>

                </springForm:form>

            </c:when>

            <c:when test="${success == 1}">
                <div class="jumbotron">
                    <div class="container">
                        <h1>Your password has been reset. Welcome back!</h1>
                        <p>You will be automatically redirected in 5 seconds.</p>
                        <meta http-equiv="refresh" content="5; url=/profile"/>
                    </div>
                </div>
            </c:when>


        </c:choose>
    </div>
</div>


<hr>

</body>
</html>