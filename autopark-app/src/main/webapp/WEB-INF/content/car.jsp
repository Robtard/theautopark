<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="tiles/head.jsp" %>

    <link rel="stylesheet" type="text/css" href="/styles/car.css" />
</head>


<body>
<%@ include file="../tags/headerV2.tag" %>

<div class="container">
        <div>
            Мой автомобиль
        </div>
        <div>
            <span title="Марка">Марка: ${car.vendor}</span>
        </div>
        <div>
            Модель: ${car.model}
        </div>
        <div>
            Год выпуска: ${car.year}
        </div>
        <div>
            Тип ТС: ${car.type}
        </div>
        <div>
            Тип ТС: ${car.capacity}
        </div>

</div>
<hr>
<%@ include file="tiles/footer.jsp" %>

</body>




