package autopark.web.controller.profile;

import autopark.domain.Role;
import autopark.dto.CarDTO;
import autopark.dto.DemandDTO;
import autopark.service.ICarService;
import autopark.service.IDemandService;
import autopark.web.auth.RequiresAuthentication;
import autopark.web.controller.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by aro on 07.03.2016.
 */
@Controller
public class MyCarsController {
    private final static Logger logger = LoggerFactory.getLogger(MyCarsController.class);

    @Autowired
    @Qualifier("carServiceJdbc")
    private ICarService carService;


    @RequestMapping(value = "/mycars")
    @RequiresAuthentication({Role.ROLE_USER,Role.ROLE_ADMIN})
    public String handle(ModelMap modelMap) {
        logger.debug("mycars");
        List<CarDTO> list = carService.getMyCars();
        modelMap.put(Constants.LIST,list);
        return "/WEB-INF/content/profile/mycars.jsp";
    }


}
