package autopark.web.controller.profile;

import autopark.dao.IUserDAO;
import autopark.domain.User;
import autopark.dto.PasswordForm;
import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.PrincipalUser;
import autopark.service.*;
import autopark.web.controller.Constants;
import autopark.web.utils.WebHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

/**
 * Created by  01 on 14.03.2016.
 */
@Controller
public class LostPasswordController {

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private ITokenService tokenService;

    @Autowired
    private IPasswordService passwordService;

    @Autowired
    private IWebService webService;

    @Autowired
    private WebHelper webHelper;

    Logger log = LoggerFactory.getLogger(LostPasswordController.class);



    @RequestMapping(value = "/lostpassword", method = RequestMethod.GET)
    public String lostPasswordGet(ModelMap modelMap, HttpServletRequest request) {

        log.info("/lostpassword GET");
        return Constants.LOST_PASSWORD;
    }

    /*class EmailForm*/


    @RequestMapping(value = "/lostpassword", method = RequestMethod.POST)
    public String lostPasswordPost(@ModelAttribute("lostpass-form") @Valid PasswordResetTokenDTO tokenDTO, Locale locale,
                                   BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {


        log.info("/lostpassword POST");

        if (bindingResult.hasErrors()) {
            return Constants.LOST_PASSWORD;
        }

        String userEmail = tokenDTO.getEmail();

        tokenDTO.setLocale(locale);

        // If we do not have this email address in the list, we should not expose this to the user.
        // This exposes that the user has an account, allowing a user enumeration attack.
        try {
            passwordService.requestResetPassword(tokenDTO, assembleBaseURL(request));
        } catch (AutoparkServiceException e) {
            modelMap.put("useremail", userEmail);
            modelMap.put("success", 1);
            return Constants.LOST_PASSWORD;
        }


        modelMap.put("useremail", userEmail);
        modelMap.put("success", 1);

        return Constants.LOST_PASSWORD;
    }


    @RequestMapping(value = "/lostpassword/finish/{token}", method = RequestMethod.GET)
    public String handleRecoverGet(ModelMap modelMap, @PathVariable(value = "token") String token) {

        /*https://stackoverflow.com/account/recover?recoveryToken=
        vIntVgAAAABZAQBzrF9PIg%3d%3d%7c1bfd1214c4e4f3dea8e2f2e47e5da77fbcf9c07836594c495c9f14491dec9482*/

        log.info("/lostpassword/finish GET");

        try {
            if (tokenService.isValidToken(token)) {

                modelMap.put("valid", 1);
                return Constants.RECOVER_PASSWORD;
            } else {
                //value: The reset key is missing.
                modelMap.put("keymissing", 1);
                return Constants.RECOVER_PASSWORD;
            }
        } catch (AutoparkServiceException e) {
            //value: Your password couldn't be reset. Remember a password request is only valid for 24 hours.
            modelMap.put("valid", 0);
        }


        return Constants.RECOVER_PASSWORD;

    }


    @RequestMapping(value = "/lostpassword/finish/{token}", method = RequestMethod.POST)
    public String handleRecoverPost(@PathVariable(value = "token") String token,
                                    @ModelAttribute("password-form") @Valid PasswordForm form,
                                    BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {

        log.info("/lostpassword/finish POST");

        if (bindingResult.hasErrors()) {
            modelMap.put("valid", 1);
            return Constants.RECOVER_PASSWORD;
        }

        String baseURL = webHelper.assembleBaseURL(request);

        PasswordResetTokenDTO tokenDTO;

        try {
            tokenDTO = passwordService.finishResetPassword(form, baseURL, token);
        } catch (AutoparkServiceException e) {
            e.printStackTrace();
            modelMap.put("valid", 1);
            modelMap.put("error", 1);
            return Constants.RECOVER_PASSWORD;
        }

        //authentication
        String email = tokenDTO.getEmail();

        User user = userDAO.getByEmail(email);

        PrincipalUser principalUser = userService.assemblePrincipalUser(user, true);
        Authentication auth = new UsernamePasswordAuthenticationToken(principalUser, null, principalUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        //value: Your password has been reset. Welcome back!
        modelMap.put("success", 1);
        return Constants.RECOVER_PASSWORD;

    }

    @ModelAttribute("lostpass-form")
    public PasswordResetTokenDTO createTokenDTO() {
        return new PasswordResetTokenDTO();
    }

    @ModelAttribute("password-form")
    public PasswordForm createPasswordFormModel() {
        return new PasswordForm();
    }


    private String assembleBaseURL(HttpServletRequest request) {
        return new StringBuilder().
                append(request.getScheme()).
                append("://").append(request.getServerName()).
                append(":").append(request.getServerPort()).
                append(request.getContextPath()).
                toString();
    }


}
