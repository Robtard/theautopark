package autopark.web.controller.profile;

import autopark.dto.ImageFileDTO;
import autopark.AutoparkRepositoryException;
import autopark.dto.UserDTO;
import autopark.service.IRecaptchaService;
import autopark.service.IUserService;
import autopark.service.IWebService;
import autopark.service.RecaptchaServiceException;
import autopark.utils.DateUtils;
import autopark.web.controller.Constants;
import autopark.web.utils.WebHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
public class SignupController {

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    @Qualifier("accountCreationValidator")
    private Validator validator;

    @Autowired
    private WebHelper webHelper;

    @Autowired
    private IWebService webService;

    @Autowired
    private IRecaptchaService recaptchaService;

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String handleSignUp(ModelMap modelMap,HttpServletRequest request) {
        return "/WEB-INF/content/signup.jsp";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String handleSignUpPost(@ModelAttribute("command") @Validated UserDTO dto,
                                   BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {
        //validation
        if (bindingResult.hasErrors()) {
            modelMap.put(Constants.COMMAND, dto);
            return Constants.SIGNUP;
        }

        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
        String remoteAddress = null;

        try {
            remoteAddress = webService.getMyIp();
        } catch (AutoparkRepositoryException e) {
            e.printStackTrace();
        }

        try {
            if (recaptchaService.isResponseValid(remoteAddress, gRecaptchaResponse)) {

                if (request instanceof MultipartRequest) {
                    //myphoto
                    MultipartRequest mRequest = (MultipartRequest) request;
                    List<MultipartFile> files = mRequest.getMultiFileMap().get("myPhoto");
                    if (files != null && files.size() > 0) {
                        ImageFileDTO imageFileDTO = new ImageFileDTO();
                        MultipartFile mpf = files.get(0);
                        if (mpf != null && mpf.getSize() > 0) {
                            imageFileDTO.setMpf(mpf);
                            dto.setImageFileDTO(imageFileDTO);
                        }
                    }
                }

                boolean valid = userService.createUser(dto, webHelper.assembleBaseURL(request));

                if (!valid) {
                    modelMap.put("res", -2);
                    return Constants.SIGNUP;
                }
            }
        } catch (RecaptchaServiceException e) {
            e.printStackTrace();
        }

        modelMap.put("res", 0);

        return Constants.SIGNUP;

    }

    @InitBinder
    private void dateBinder(WebDataBinder binder) {
        //Create a new CustomDateEditor
        CustomDateEditor editor = new CustomDateEditor(DateUtils.DDMMYYYY, true);
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Date.class, editor);

        //Add validator
        binder.setValidator(validator);

    }

    @ModelAttribute("command")
    public UserDTO createEmployeeModel() {
        return new UserDTO();
    }

}



