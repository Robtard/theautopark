package autopark.service;


/**
 * 01.04.2016.
 */
public interface IRecaptchaService {

    boolean isResponseValid(String ipAdress, String response) throws RecaptchaServiceException;
}
