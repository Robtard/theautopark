package autopark.service;

import org.springframework.web.client.RestClientException;

/**
 * 01.04.2016.
 */
public class RecaptchaServiceException extends Throwable {
    public RecaptchaServiceException(String s, RestClientException e) {
        super(s, e);
    }
}
