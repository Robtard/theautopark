package autopark.service.impl;

import autopark.dao.IUserDAO;
import autopark.dao.ImageFileDAO;
import autopark.domain.ImageFile;
import autopark.domain.User;
import autopark.dto.ImageFileDTO;
import autopark.service.AutoparkServiceException;
import autopark.service.IUserService;
import autopark.service.ImageService;
import autopark.utils.ApIOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

/**
 * 21.03.2016.
 */

@Service
public class ImageServiceImpl implements ImageService {
    private final Logger log = LoggerFactory.getLogger(ImageServiceImpl.class);

    public static final String UPLOAD_FILE_FOLDER = "/japp/autopark/upload";


    private static int[] fileLock = new int[1];
    public static final Long fileSizeLimit = 12000000l;  //about 10 mb

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private ImageFileDAO imageFileDAO;

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    /**
     * Метод который сохраняет файл
     * @param dto
     * @return
     * @throws AutoparkServiceException
     */
    public ImageFile uploadFile(ImageFileDTO dto) throws AutoparkServiceException {
        User user = null;
        try {
            user = userService.getCurrentUser();
        } catch (AutoparkServiceException e) {
            log.error("No user");
        }


        synchronized (fileLock){
            MultipartFile mpf = dto.getMpf();

            ImageFile tempFile = null;
            if (mpf != null) {
                long byteSize = mpf.getSize();
                if(byteSize>fileSizeLimit){
                    throw new AutoparkServiceException("file so big");
                }
                try {
                    tempFile = new ImageFile();
                    tempFile.setFileName(mpf.getOriginalFilename());
                    tempFile.setDate(new Date());
                    if(user!=null){
                        tempFile.setUserId(user.getId());
                        tempFile.setUserEmail(user.getEmail());
                    }

                    Long id = imageFileDAO.save(tempFile);

                    String filePath = createFile(id);
                    if(filePath == null){
                        filePath = createFile(id);
                    }
                    if(filePath==null){
                        throw new AutoparkServiceException("cannot create the file on disk, id = "+id);
                    }
                    tempFile.setPathfolder(UPLOAD_FILE_FOLDER+"/");
                    imageFileDAO.merge(tempFile);


                    ApIOUtils.saveInputStreamAsFile(mpf.getInputStream(), filePath);
                    dto.setId(id);
                    dto.setFileName(tempFile.getFileName());
                    dto.setMpf(null);

                } catch (Exception e) {
                    log.error("uploadFileFromAjax error",e);
                    tempFile = null;
                    e.printStackTrace();
                }finally {
                    //audit
                }
                mpf=null;
                dto.setMpf(null);
            }
            return tempFile;
        }
    }

    private String createFile(Long id){
        String startFolder = UPLOAD_FILE_FOLDER;
        String fullFolder = startFolder+"/"+id +"/";
        java.io.File startFolderF = new java.io.File(startFolder);
        startFolderF.mkdirs();
        String filePath = fullFolder + "tf.f";
        java.io.File fileOnDisk = new java.io.File(filePath);
        try {
            fileOnDisk.createNewFile();
        } catch (IOException e) {
            log.error("cannot store file on disk", e);
            return null;
        }

        return filePath;

    }

    @Override
    public boolean uploadMyPhoto(ImageFileDTO dto) throws AutoparkServiceException {
        User user = null;
        try {
            user = userService.getCurrentUser();
        } catch (AutoparkServiceException e) {
            e.printStackTrace();
            throw new AutoparkServiceException("Not authenticated user");
        }

        ImageFile imageFile = uploadFile(dto);
        if(imageFile!=null){
            user.setMyPhotoImageFileId(imageFile.getId());
            userDAO.merge(user);
        }
        return true;
    }
}
