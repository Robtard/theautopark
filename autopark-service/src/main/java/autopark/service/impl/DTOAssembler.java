package autopark.service.impl;

import autopark.domain.Car;
import autopark.domain.Demand;
import autopark.domain.User;
import autopark.dto.CarDTO;
import autopark.dto.DemandDTO;
import autopark.dto.UserDTO;

/**
 * Created by aro on 26.03.2016.
 */
public class DTOAssembler {

    public static UserDTO assembleUserDTO(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setEmail(user.getEmail());
        userDTO.setCreationDate(user.getCreationDate());
        userDTO.setBirthday(user.getBirthday());
        userDTO.setPhone(user.getPhone());
        userDTO.setMyPhotoImageFileId(user.getMyPhotoImageFileId());
        return userDTO;
    }

    public static User assembleUser(UserDTO dto){
        User user = new User();
        user.setName(dto.getName());
        user.setEmail(dto.getEmail());
        user.setCreationDate(dto.getCreationDate());
        user.setBirthday(dto.getBirthday());
        user.setPhone(dto.getPhone());
        return user;
    }

    public static DemandDTO assembleDemandDTO(Demand demand){
        DemandDTO dto = new DemandDTO();
        dto.setId(demand.getId());
        dto.setType(demand.getType());
        dto.setCapacity(demand.getCapacity());
        dto.setCreationDate(demand.getCreationDate());
        dto.setDescription(demand.getDescription());
        dto.setSettlement(demand.getSettlement());
        dto.setStartWorkDate(demand.getStartWorkDate());
        return dto;
    }

    public static CarDTO assembleCarDTO(Car car){
        CarDTO dto = new CarDTO();
        dto.setCapacity(car.getCapacity());
        dto.setDescription(car.getDescription());
        dto.setModel(car.getModel());
        dto.setType(car.getType());
        dto.setYear(car.getYear());
        dto.setVendor(car.getVendor());
        dto.setId(car.getId());
        if(car.getUser()!=null){
            dto.setUser(assembleUserDTO(car.getUser()));
        }
        return dto;
    }

}
