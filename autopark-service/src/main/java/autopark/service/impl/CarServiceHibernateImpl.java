package autopark.service.impl;

import autopark.dao.ICarDAO;
import autopark.domain.Car;
import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
@Service("carServiceHibernate")
public class CarServiceHibernateImpl implements ICarService{

    @Autowired
    private ICarDAO carDAO;


    public List<CarDTO> getCars() {
        List<Car> cars = carDAO.getCarsHibernate();
        List<CarDTO> resultList = null;
        if(cars != null){
            resultList = new ArrayList<CarDTO>();
            for(Car car: cars){
                resultList.add(DTOAssembler.assembleCarDTO(car));
            }
        }
        return resultList;
    }

    public CarDTO getCar(Long id) {
        Car car = carDAO.get(id);
        return DTOAssembler.assembleCarDTO(car);
    }

    @Override
    public List<CarDTO> searchCars(CarSearchDTO dto) {
        return null;
    }

    public List<CarDTO> getMyCars() {
        return null;
    }
}
