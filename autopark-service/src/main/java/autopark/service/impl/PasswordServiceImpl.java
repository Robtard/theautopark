package autopark.service.impl;

import autopark.dao.IPasswordResetTokenDAO;
import autopark.dao.IUserDAO;
import autopark.domain.PasswordResetToken;
import autopark.domain.User;
import autopark.dto.PasswordForm;
import autopark.dto.PasswordResetTokenDTO;
import autopark.service.AutoparkServiceException;
import autopark.service.IEmailSender;
import autopark.service.IPasswordService;
import autopark.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * 22.03.2016.
 */

@Service
public class PasswordServiceImpl implements IPasswordService {

    Logger log = LoggerFactory.getLogger(PasswordServiceImpl.class);

    @Autowired
    private IEmailSender emailSender;

    /*@Autowired
    private PasswordResetTokenJpaRepository tokenRepository;*/
    @Autowired
    private IPasswordResetTokenDAO tokenDAO;

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    BCryptPasswordEncoder encoder;

    @Override
    @Transactional
    public void requestResetPassword(PasswordResetTokenDTO tokenDTO, String baseURL) throws AutoparkServiceException {

        String email = tokenDTO.getEmail();

        User user = userDAO.getByEmail(email);
        if (user != null) {

            String tokenValue = UUID.randomUUID().toString();

            tokenDTO.setTokenValue(tokenValue);
            tokenDTO.setName(user.getName());

            Date currentDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentDate);
            calendar.add(Calendar.HOUR_OF_DAY, 1);
            Date expirationDate = calendar.getTime();

            PasswordResetToken tokenEntity = new PasswordResetToken();
            tokenEntity.setDate(expirationDate);
            tokenEntity.setEmail(tokenDTO.getEmail());
            tokenEntity.setTokenValue(tokenValue);

            log.info("Saving token...");

            tokenDAO.save(tokenEntity);
            //tokenRepository.save(tokenEntity);

            emailSender.sendPasswordResetMail(baseURL, tokenDTO);

        } else {
            throw new AutoparkServiceException("User not found");
        }
    }


    @Override
    @Transactional
    public PasswordResetTokenDTO finishResetPassword(PasswordForm form, String baseURL, String tokenValue) throws AutoparkServiceException {

        log.info("Reset user password for reset key {}", tokenValue);
        PasswordResetToken token = tokenDAO.findOneByTokenValue(tokenValue);

        if (token != null) {
            User user = userDAO.getByEmail(token.getEmail());
            if (user != null) {
                String encryptedPassword = encoder.encode(form.getPassword());
                user.setPassword(encryptedPassword);

                log.info("Updating user password");
                userDAO.update(user);

                tokenDAO.delete(token);

                PasswordResetTokenDTO tokenDTO = new PasswordResetTokenDTO();
                tokenDTO.setName(user.getName());
                tokenDTO.setEmail(token.getEmail());

                log.info("Sending email for user");
                emailSender.sendSuccessPasswordResetMail(baseURL, tokenDTO);

                return tokenDTO;

            } else {
                throw new AutoparkServiceException("User not found!");
            }
        }
        return new PasswordResetTokenDTO();
    }
}
