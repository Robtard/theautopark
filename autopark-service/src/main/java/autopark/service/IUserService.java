package autopark.service;

import autopark.domain.User;
import autopark.dto.PrincipalUser;
import autopark.dto.UserDTO;

public interface IUserService {
    boolean createUser(UserDTO dto, String baseURL);

    UserDTO getUserByEmail(String email);

    boolean updateUserPassword(UserDTO userDTO, String baseURL);

    PrincipalUser assemblePrincipalUser(User user, Boolean facebook);

    UserDTO getUser();

    User getCurrentUser() throws AutoparkServiceException;

}
