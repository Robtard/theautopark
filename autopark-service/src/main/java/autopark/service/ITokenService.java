package autopark.service;

/**
 * 21.03.2016.
 */
public interface ITokenService {

    boolean isValidToken(String tokenValue) throws AutoparkServiceException;

}
