package autopark.service;

import autopark.dto.PasswordForm;
import autopark.dto.PasswordResetTokenDTO;

/**
 * 22.03.2016.
 */
public interface IPasswordService {

    void requestResetPassword(PasswordResetTokenDTO tokenDTO, String baseURL) throws AutoparkServiceException;

    PasswordResetTokenDTO finishResetPassword(PasswordForm form, String baseURL, String tokenValue) throws AutoparkServiceException;

}
