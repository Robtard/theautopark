package autopark.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by vclass20 on 08.02.2016.
 *
 * http://www.baeldung.com/jackson-serialize-enums  - хорошая ссылка по enum and json
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CarTypeEnum {
    TRUCK("Фура"),
    TIPPER("Самосвал"),
    CRANE("Кран"),
    FRIG("Холодильник"),
    CRISPER("Чёто");

    private CarTypeEnum(String displayName){
        this.displayName = displayName;
    }

    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name();
    }


}
